<?php

/* Config  */
/* https://github.com/vicenteguerra/git-deploy/blob/master/deploy.sample.php */
define("TOKEN", "secret-token");                                                // The secret token to add as a GitHub or GitLab secret, or otherwise as https://www.example.com/?token=secret-token
define("REMOTE_REPOSITORY", "git@gitlab.com:haikumods/hk_request_limiter.git"); // The SSH URL to your repository
define("DIR", "/var/www/clients/client3/web21/web/modules/hk_request_limiter/");// The path to your repostiroy; this must begin with a forward slash (/)
define("BRANCH", "refs/heads/main");                                            // The branch route
define("LOGFILE", "git.webhook.log");                                        // The name of the file you want to log to.
define("GIT", "/usr/bin/git");                                                  // The path to the git executable
define("MAX_EXECUTION_TIME", 180);                                              // Override for PHP's max_execution_time (may need set in php.ini)
define("BEFORE_PULL", "");                                                      // A command to execute before pulling
define("AFTER_PULL", "");                                                       // A command to execute after successfully pulling

require_once("git.webhook.core.php");
